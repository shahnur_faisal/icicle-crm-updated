<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Investor;
use App\Models\InvestorPay;
use App\Models\Invoice;
use App\Models\Maintenance;
use App\Models\Service;
use Illuminate\Http\Request;
use DB;

class AjaxController extends Controller
{
    //getName
    public function getName()
    {
        $id = request()->serial_No;

        // $investorPay=InvestorPay::where('investor_id',$id)->latest()->first();
        // $paidMonth=$investorPay->end_month ?? 'null';
        $invoice = Invoice::with('customer','services')->where('customer_id', $id)->first();
        $invoicePay = InvestorPay::where('customer_id',$invoice->customer_id)->sum('total');
        $invPaid = $invoicePay + $invoice->advance;
        $invDue = $invoice->dueAmount - $invoicePay;
        return response()->json([$invoice,$invPaid,$invDue]);

    }
    //getPrice
    public function getPrice()
    {
        $id = request()->service_id;
        $service=Service::where('id',$id)->first();
        return response()->json([$service]);
    }
    //getMaintenanceAmount
    public function getMaintenanceAmount(){
        $customer_id = request()->serial_No;
        $maintenance = Maintenance::with('customer')->where('id', $customer_id)->first();
        return response()->json($maintenance);
    }
    //getPermission
    public function getPermission(Request $request)
    {
        $module_id = $request->post('module_id');
        $subModule = DB::table('sub_modules')->where('module_id', $module_id)->get();
        $html = '<option value="">Select A Sub Module</option>';
        foreach ($subModule as $item) {
            $html .= '<option value="' . $item->id . '">' . $item->subModule_name . '</option>';
        }
        return response()->json($html);
    }
}
