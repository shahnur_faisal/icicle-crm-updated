<?php

namespace App\Http\Controllers;

use App\Models\CashInHand;
use App\Models\Category;
use App\Models\Employee;
use App\Models\Expense;
use App\Models\InvestorPay;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ExpenseController extends Controller
{
    //expenselist
    public function expenselist(){
        $data = array();
        $data['active_menu'] = 'expence';
        $data['page_title'] = 'Expense List';
        $expense = Expense::all();
        return view('backend.expense.expense_list',compact('expense','data'));
    }
    //expensePost
    public function expensePost(Request $request){
        $data = array();
        $data['active_menu'] = 'expence';
        $data['page_title'] = 'Expense List';
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $expense = Expense::whereDate('created_at', '>=', $from_date)->whereDate('created_at', '<=', $to_date)->get();
        return view('backend.expense.expense_list',compact('expense','data'));
    }
    //expenseCreate
    public function expenseCreate(){
        $data = array();
        $data['active_menu'] = 'expence';
        $data['page_title'] = 'Expense Create';
        $invoice = Invoice::sum('advance');
        $expense =  Expense::sum('amount');
        $invoicePay = InvestorPay::sum('total');
        $cashhand = CashInHand::sum('amount');
        $totalIncome = ($invoice ?? 0) + ($invoicePay ?? 0) + ($cashhand ?? 0);
        $cashInHand = $totalIncome - $expense;
        $employee = Employee::all();
        $category = Category::all();
        if(request()->isMethod('post')){
            if (request()->hasFile('file')) {
                $extension = request()->file('file')->getClientOriginalExtension();
                $fileName = 'backend/img/expense/'.uniqid().'.'.$extension;
                request()->file('file')->move('backend/img/expense',$fileName);
            }
            $expense = new Expense();
            $expense->date = request()->date;
            $expense->category_id  = request()->category_id;
            $expense->employee_id  = request()->employee_id;
            $expense->note  = request()->note;
            $expense->currency  = request()->currency;
            $expense->paymentMode  = request()->paymentMode;
            $expense->amount  = request()->amount;
            $expense->file  = $fileName;
            $expense->save();
            return back()->with('message','Expense Successfully Added');
        }
        return view('backend.expense.createExpense',compact('employee','category','data','totalIncome','cashInHand'));
    }
    public function expenseDestroy($id)
    {
        $expense = Expense::find($id);
        $file = $expense->file;
        if(File::exists($file)){
            File::delete($file);
        }
        $expense->delete();
        return back()->with('message','Expense Successfully Deleted');

    } 
}
