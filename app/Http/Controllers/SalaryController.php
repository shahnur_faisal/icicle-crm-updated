<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Salary;
use Illuminate\Http\Request;

class SalaryController extends Controller
{
    //salary
    public function salary(){
        $data = array();
        $data['active_menu'] = 'salaryAdd';
        $data['page_title'] = 'Add Salary';
        $employee = Employee::select('id','name','designation')->get();
        if (request()->isMethod('post')) {
            $salary = new Salary();
            $salary->employee_id = request()->employee_id;
            $salary->deliveravbles = request()->deliveravbles;
            $salary->save();
            return back()->with('message','Salary Added Successfully');
        }
        return view('backend.salary.addSalary',compact('data','employee'));
    }
}
