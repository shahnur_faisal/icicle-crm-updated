<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Employee;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [];
        $data['active_menu'] = 'task_assign';
        $data['page_title'] = 'Task Assign';
        $task = Task::all();
        $employee = Employee::all();
        $customer = Customer::all();
        $auth = Auth::guard('admin')->user()->id;
        $employees = Employee::where('authId',$auth)->first();
        if ($employees) {
            $task = Task::where('employee_id',$employees->authId)->get();
            return view('backend.task.task', compact('data', 'task', 'employee', 'customer'));
        }
        return view('backend.task.task', compact('data', 'task', 'employee', 'customer'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Task::create([
            'customer_id' => $request->customer_id,
            'description' => $request->description,
            'title' => $request->title,
            'employee_id' => $request->employee_id,
            'assign_date' => $request->assign_date,
            'end_date' => $request->end_date,
            'status' => $request->status,
        ]);
        $customer = Customer::find($request->customer_id);
        $customer->status = 'ongoing';
        $customer->save();
        return back()->with('Task Created Successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = [];
        $data['active_menu'] = 'task_assign';
        $data['page_title'] = 'Task Assign';
        $task = Task::find($id);
        $employee = Employee::all();
        return view('backend.task.taskEdit', compact('employee', 'task', 'data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $task = Task::find($id);
        $task->update([
            'title' => $request->title,
            'description' => $request->description,
            'employee_id' => $request->employee_id,
            'title' => $request->title,
            'assign_date' => $request->assign_date,
            'end_date' => $request->end_date,
            'status' => $request->status,
        ]);
        return back()->with('Task Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $task = Task::find($id);
        $task->delete();
        return back()->with('message', 'Task Deleted Successfully');
    }
    public function taskStatus($id)
    {
        $task = Task::find($id);
        if ($task->status == 'active') {
            $task->update([
                'status' => 'inactive',
            ]);
        } else {
            $task->update([
                'status' => 'active',
            ]);
        }
        return back()->with('message', 'Status Changed Successfully');
    }
    //taskProgressStatus
    public function taskProgressStatus($id)
    {
        $task = Task::find($id);
        $task->update([
            'progress_status' => 'complete',
        ]);
        $customer = Customer::where('id', $task->customer_id)->first();
        $customer->status = 'completed';
        $customer->save();
        return back()->with('message', 'Status Changed Successfully');
    }
    //maintenance
    public function maintenance($id)
    {
        $task = Task::find($id);
        $task->update([
            'progress_status' => 'maintenance',
        ]);
        $customer = Customer::where('id', $task->customer_id)->first();
        $customer->status = 'maintenance';
        $customer->save();
        return back()->with('message', 'Status Changed Successfully');
    }
}
