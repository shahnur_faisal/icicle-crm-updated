@extends('backend.partials.app')
@push('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-lg-12 mb-4 order-0">
            <div class="card">
                <div class="d-flex align-items-end row">
                    <div class="col-sm-12">
                        <div class="card-body">

                            <div class="row">
                                <!-- Basic Layout -->
                                <div class="col-xxl">
                                    <div class="card mb-2">
                                        <div class="card-body">
                                            @if (session('success'))
                                            <div class="alert slert-success timeout" style="color: green">{{ session('success') }}</div>
                                            @elseif (session('error'))
                                            <div class="alert slert-danger timeout">{{ session('error') }}</div>
                                            @endif
                                            <form method="post" enctype="multipart/form-data" onsubmit="return checkAmount()">
                                                @csrf

                                                <div class="row my-2">
                                                    <div class="d-flex justify-content-between">
                                                        <div>
                                                            <h2>New Expense</h2>
                                                        </div>
                                                        <div>
                                                            <a href="" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#exampleModal">Boost Cash In Hand</a>

                                                          
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label for="employee_id" class="my-2">Staff</label>
                                                        <select class="form-control" name="employee_id" id="employee_id">
                                                            <option value="">Select</option>
                                                            @foreach ($employee as $item)
                                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="category_id" class="my-2">Expense Category</label>
                                                        <select class="form-control" name="category_id" id="category_id">
                                                            <option value="">Select</option>
                                                            @foreach ($category as $item)
                                                            <option value="{{ $item->id }}">{{ $item->category_name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="amount" class="my-2">Cash In Hand</label>
                                                        <input type="number" name="totalIncome" value="{{ $cashInHand }}" id="totalIncome" class="form-control" readonly><br>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="amount" class="my-2">Amount</label>
                                                        <input type="number" name="amount" id="amount" class="form-control"><br>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="currency" class="my-2">Currency</label>
                                                        <select class="form-control" name="currency" id="currency">
                                                            <option value="">Select</option>
                                                            <option value="AUD">AUD</option>
                                                            <option value="GBP">GBP</option>
                                                            <option value="USD">USD</option>
                                                            <option value="BDT">BDT</option>
                                                        </select>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="paymentMode" class="my-2">Payment Mode</label>
                                                        <select class="form-control" name="paymentMode" id="paymentMode">
                                                            <option value="">Select</option>
                                                            <option value="Bank">Bank</option>
                                                            <option value="Online">Online</option>
                                                            <option value="Cheque">Cheque</option>
                                                            <option value="Cash">Cash</option>
                                                        </select>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="name_or_business" class="my-2">Attach Receipt</label>
                                                        <input type="file" name="file" id="file" class="form-control">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="name" class="my-2">Date</label>
                                                        <input type="date" name="date" id="date" class="form-control"><br>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label for="note" class="my-2">Note</label>
                                                        <textarea name="note" id="note" class="form-control"></textarea><br>
                                                    </div>
                                                </div>


                                                <div class="text-center">
                                                    <input type="submit" value="Submit" class="btn btn-primary">
                                                </div>
                                                <hr>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Boost Cash In hand</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('cashInHand') }}" method="post">
                @csrf
                <div class="modal-body">
                    <input type="number" name="amount" id="amount" placeholder="Amount" class="form-control">
                </div>
                <div class="modal-body">
                    <textarea class="form-control" name="note" id="note" cols="" rows="" placeholder="note"></textarea>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    function checkAmount() {
        var cashInHand = parseFloat(document.getElementById('totalIncome').value);
        var amount = parseFloat(document.getElementById('amount').value);
        if (amount > cashInHand) {
            alert("Amount cannot be greater than Cash In Hand!");
            return false;
        }
        return true;
    }

</script>

@endpush
